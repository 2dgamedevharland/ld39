#Ludum Dare 39#

Hello and welcome to this Ludum Dare Project.
We developed a 2D RGP Style Pixel Game based on the [MonoGame](http:www.monogame.net) Libary.

###Tools we used###
* [MonoGame](http:www.monogame.net) (V 3.6)
* [Visual Studio 2017](https://www.visualstudio.com/vs/community)
* [Tiled Map](http://www.mapeditor.org/) (XML Tile-Map-Editor)
* [Magix Music Maker 2017](http://www.magix.com/index.php?id=20772&L=52&AffiliateID=125&phash=NxSG8i2Td8oCJz3I&utm_source=Bing&utm_medium=cpc&utm_campaign=Brand_Product_Audio_US)
* [Git Kraken](https://www.gitkraken.com) (Git-Tool)

###The Game###

You control a man trapped in a castle full of snakes.
Sadly you got bitten and your life drains out of your body.
Hurry! Find the entrance and take the potion you left there.

Meanwhile kill some snkaes :) 

###Control###

Arrow Keys - Move
Anything else - See Screendescription

###Developer-Team###

Josia U.
[Silas U.](http://ludumdare.com/compo/author/mennchen22/)

###Used Things###

* [Walking-Man-Sprite](https://opengameart.org/content/spritesheet-of-a-man)
* [Healthbar-1](https://opengameart.org/content/pseudo-a-25d-roguelike-robot-sprite-sheet-0)
* [Healthbar-2](https://opengameart.org/content/health-and-manabars-100x12px-200x24px)
* [Snake](http://spritedatabase.net/file/6556)

###Total Time###
25 Hours coding

**_Please feel free to play the game and leave a comment :)_**