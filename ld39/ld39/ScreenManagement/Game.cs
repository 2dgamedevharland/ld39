﻿using System.Diagnostics;
using Microsoft.Xna.Framework;


namespace ld39
{
    public class Game : Microsoft.Xna.Framework.Game
    {
        private static GraphicsDeviceManager graphics;
        ScreenManager screenManager;

        public Game()
        {
            Content.RootDirectory = "Content";

            graphics = new GraphicsDeviceManager(this);

            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 600;

            // Create the screen manager component.
            screenManager = new ScreenManager(this);

            Components.Add(screenManager);

            // Activate the first screens.
            screenManager.AddScreen(new StartScreen(), null);
            
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            base.Draw(gameTime);
        }

        public static  void toggleFullScreen()
        {
            Debug.WriteLine("Set to FullScreen");
           graphics.ToggleFullScreen();
        }
    }
}
