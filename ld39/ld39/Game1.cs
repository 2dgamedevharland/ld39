﻿using System.Diagnostics;
using ld39;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using RogueSharp;
using TiledSharp;

namespace ld39
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        private MapManager mapManager;
        private PathToPlayer pathToPlayer;
        private Cell walkTo;
        private InputState _inputState;
        private AggressiveEnemy _aggressiveEnemy;
      

        // Represents the player

        Player player;
        HealthBarAnimation playerHealthBarAnimation;

        // Keyboard states used to determine key presses

        KeyboardState currentKeyboardState;
        KeyboardState previousKeyboardState;



        // Gamepad states used to determine button presses

        GamePadState currentGamePadState;
        GamePadState previousGamePadState;



        //Mouse states used to track Mouse button press

        MouseState currentMouseState;
        MouseState previousMouseState;

        // A movement speed for the player

        float playerMoveSpeed;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = Window.ClientBounds.Width;
            graphics.PreferredBackBufferHeight = Window.ClientBounds.Height;
            

            //set the GraphicsDeviceManager's fullscreen property
            graphics.IsFullScreen = false;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            // Initialize the player class
            Global.Camera.ViewportWidth = graphics.GraphicsDevice.Viewport.Width;
            Global.Camera.ViewportHeight = graphics.GraphicsDevice.Viewport.Height;
           
           
          
            // Set a constant player move speed

            
           
            //Enable the FreeDrag gesture.

            TouchPanel.EnabledGestures = GestureType.FreeDrag;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            TmxMap map = new TmxMap("Content/tiledmap.tmx");
            Texture2D mapsprite = Content.Load<Texture2D>("map/dungeon");
            Texture2D white = Content.Load<Texture2D>("map/white");
            Texture2D mouseCourser = Content.Load<Texture2D>("map/Courser");

            mapManager = new MapManager(map, mapsprite);
           
            _inputState = new InputState(mouseCourser);
            // TODO: use this.Content to load your game content here

            // Load the player resources

            playerHealthBarAnimation = new HealthBarAnimation();
            Texture2D playerHealthBarTexture = Content.Load<Texture2D>("player\\Leben");
            playerHealthBarAnimation.Initialize(playerHealthBarTexture, 244, 61, 8, 90, Color.White, 1f, true);

            Animation playerAnimation = new Animation();
            Texture2D playerTexture = Content.Load<Texture2D>("player\\Sturmi_komplettNEU");
            playerAnimation.Initialize(playerTexture, Vector2.Zero, 64, 64, 3, 90, Color.White, 1f, true);

            Animation playerRIP = new Animation();
            Texture2D playerRIPTexture = Content.Load<Texture2D>("player\\RIP");
            playerRIP.Initialize(playerRIPTexture, Vector2.Zero, 64, 64, 7, 180, Color.White, 1f, false);


            Vector2 playerPosition = new Vector2(
                GraphicsDevice.Viewport.TitleSafeArea.X + GraphicsDevice.Viewport.TitleSafeArea.Width / 2,
                GraphicsDevice.Viewport.TitleSafeArea.Y + GraphicsDevice.Viewport.TitleSafeArea.Height / 2);


            Cell startPosition = new Cell(4,5,true,true,true);
            player = new Player(GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, startPosition);
            pathToPlayer = new PathToPlayer(player, mapManager.getCellMap(), white);
            Vector2 pos = new Vector2((int) startPosition.X * playerAnimation.FrameWidth, (int) startPosition.Y * playerAnimation.FrameHeight);

            Debug.WriteLine("Startpos.: "+ startPosition.X+","+startPosition.Y+ " --- "+ pos);
            player.Initialize(playerAnimation, pos, playerRIP);
            mapManager.Update(startPosition);
            Global.Camera.CenterOn(player.PlayerPosition);

            Texture2D enemy = Content.Load<Texture2D>("player\\Snake");
            Animation enemyAnimation = new Animation();
            enemyAnimation.Initialize(enemy,pos,45,80,32,90,Color.White,1.0f,true);

            _aggressiveEnemy = new AggressiveEnemy(pathToPlayer)
            {
                X = startPosition.X,
                Y = startPosition.Y,
                Scale = 1f,
                Sprite = enemyAnimation
            };

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            _inputState.Update();
            
            Global.Camera.CenterOn(player.PlayerPosition);
            if (_inputState.IsExitGame(PlayerIndex.One))
            {
                Exit();
            }
            if (walkTo == null)
            {
                walkTo = player.PlayerPosition;
            }
            
            //Update the Map FOV 
            mapManager.Update(player.PlayerPosition);


            //Update the player
            player.UpdatePlayer(gameTime, _inputState ,mapManager.getCellMap());

            //Update Enemy
            _aggressiveEnemy.Update(gameTime);

            //Calculate path to the last mouse click
            if (_inputState.IsNewLeftMouseClick(out currentMouseState))
            {
                walkTo = _inputState.GetMousePositionCell(new Vector2(currentMouseState.X, currentMouseState.Y),mapManager.getCellMap(),player.Width);
            }

            pathToPlayer.CreateFrom(walkTo.X, walkTo.Y);
            playerHealthBarAnimation.Update(gameTime,player.Health);
            base.Update(gameTime);
        }

    

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend,
                null, null, null, null, Global.Camera.TranslationMatrix);
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here
            mapManager.Draw(spriteBatch, player.PlayerPosition);
            pathToPlayer.Draw(spriteBatch);
           
            
            _inputState.DrawMouseCourser(spriteBatch);
            player.Draw(spriteBatch);
            _aggressiveEnemy.Draw(spriteBatch);
            playerHealthBarAnimation.Draw(spriteBatch);

            spriteBatch.End();

   
            base.Draw(gameTime);
        }

       
    }


   
}
