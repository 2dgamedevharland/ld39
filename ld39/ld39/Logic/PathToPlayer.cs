﻿

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueSharp;

namespace ld39
{
    public class PathToPlayer
    {
        private readonly Player _player;
        private readonly IMap _map;
        private readonly Texture2D _sprite;
        private readonly PathFinder _pathFinder;
        private Path _cells;

        public PathToPlayer(Player player, IMap map, Texture2D sprite)
        {
            _player = player;
            _map = map;
            _sprite = sprite;
            _pathFinder = new PathFinder(map);
        }
        public Cell FirstCell
        {
            get { return (Cell) _cells.Steps.First(); }
        }

        public Cell NextStepCell
        {
            get
            {
                if (_cells.CurrentStep.Equals(_cells.End))
                {
                    return (Cell)_cells.End;
                }
                return (Cell)_cells.StepForward();
            }
        }

        public void CreateFrom(int x, int y)
        {
            Cell playerC = _player.PlayerPosition;
            try
            {
                _cells = _pathFinder.ShortestPath(_map.GetCell(x, y),
                    _map.GetCell(playerC.X, playerC.Y));
            }
            catch (PathNotFoundException e)
            {
                Cell plC = _player.PlayerPosition;
                CreateFrom(plC.X, plC.Y);
            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            if (_cells != null)
            {
                foreach (Cell cell in _cells.Steps)
                {
                    if (cell != null)
                    {
                        float scale = 1.0f;
                        float multiplier = 1.0f * _sprite.Width;
                        spriteBatch.Draw(_sprite, new Vector2(cell.X * multiplier, cell.Y * multiplier),
                            null, null, null, 0.0f, new Vector2(scale, scale), Color.Blue * .2f,
                            SpriteEffects.None, 0.6f);
                    }
                }
            }
        }

        internal Path getPath()
        {
            return _cells;
        }
    }
}
