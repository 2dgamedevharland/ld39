﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ld39.Logic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ld39.Screens
{
    public class CombatManager
    {
        private readonly Player _player;
        private readonly List<AggressiveEnemy> _aggressiveEnemies;
        private Dialog dialog;

        // When we construct the CombatManager class we want to pass in references
        // to the player and the list of enemies.
        public CombatManager(Player player, List<AggressiveEnemy> aggressiveEnemies, SpriteFont font)
        {
            _player = player;
            _aggressiveEnemies = aggressiveEnemies;
            dialog = Dialog.getInstance(font);
        }

        // Use this method to resolve attacks between Figures
        public void Attack(Figure attacker, Figure defender)
        {
            if (defender.Health <= 0)
            {
                return;
            }

            // First create a twenty-sided die
            var attackDie = Global.Random.Next(20);
            // Roll the die, add the attack bonus, and compare to the defender's armor class
            if (attackDie + attacker.AttackBonus >= defender.ArmorClass)
            {
                // Roll damage dice and sum them up
                int damage = attacker.Damage;
                // Lower the defender's health by the amount of damage
                defender.Health -= damage;
                // Write a combat message to the debug log.
                // Later we'll add this to the game UI
                Debug.WriteLine("{0} hit {1} for {2} and he has {3} health remaining.",
                    attacker.Name, defender.Name, damage, defender.Health);

                if (attacker is Player)
                {
                    dialog.addDialog("Deal " + damage + " Dmg", Color.Blue);
                }
                else
                {
                    dialog.addDialog("-" + damage + " HP", Color.DarkRed);
                }


                if (defender.Health <= 0)
                {
                    if (defender is AggressiveEnemy)
                    {
                        var enemy = defender as AggressiveEnemy;
                        // When an enemies health dropped below 0 they died
                        // Remove that enemy from the game
                        _aggressiveEnemies.Remove(enemy);
                    }
                    // Later we'll want to display this kill message in the UI
                    Debug.WriteLine("{0} killed {1}", attacker.Name, defender.Name);

                    if (attacker is Player)
                    {
                        dialog.addDialog("Killed " + defender.Name , Color.OrangeRed);
                    }
                    else
                    {
                        dialog.addDialog("" + attacker.Name + " Killed You", Color.DarkRed);
                    }
                }
            }
            else
            {
                // Show the miss message in the Debug log for now
                Debug.WriteLine("{0} missed {1}", attacker.Name, defender.Name);
            }
        }

        // Helper method which returns the figure at a certain map cell
        public Figure FigureAt(int x, int y)
        {
            if (IsPlayerAt(x, y))
            {
                return _player;
            }
            return EnemyAt(x, y);
        }

        // Helper method for checking if the player is at a map cell
        public bool IsPlayerAt(int x, int y)
        {
            return (_player.X_Cell == x && _player.Y_Cell == y);
        }

        // Helper method for getting an enemy at a map cell
        public AggressiveEnemy EnemyAt(int x, int y)
        {
            foreach (var enemy in _aggressiveEnemies)
            {
                if (enemy.X == x && enemy.Y == y)
                {
                    return enemy;
                }
            }
            return null;
        }

        // Helper method for checking if an enemy is at a map cell
        public bool IsEnemyAt(int x, int y)
        {
            return EnemyAt(x, y) != null;
        }
    }
}