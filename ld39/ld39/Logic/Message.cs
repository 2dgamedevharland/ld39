﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ld39.Logic
{
    class Message
    {
        private string messagetext;
        private Color color;
        private float scale;
        private Vector2 randomPosition;

        public Message(string message, Color color)
        {
            this.messagetext = message;
            this.Color = color;
            this.Scale = Global.Random.Next(7) * 0.2f;
            if (scale < 0.7f)
            {
                scale = 0.7f;
            }else if (scale > 1.0f)
            {
                scale = 1.0f;
            }
            randomPosition = new Vector2(0,0);
            this.randomPosition.X = Global.Random.Next(30);
            this.randomPosition.Y = Global.Random.Next(30);
        }

        public Color Color { get => color; set => color = value; }
        public string MessageText { get => messagetext; set => messagetext = value; }
        public float Scale { get => scale; set => scale = value; }
        public Vector2 RandomPosition { get => randomPosition; set => randomPosition = value; }
    }
}
