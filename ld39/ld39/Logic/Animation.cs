﻿

using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ld39
{
    public class Animation
    {

        public static int MOVE_UP = 2;
        public static int MOVE_DOWN = 0;
        public static int MOVE_LEFT = 3;
        public static int MOVE_RIGHT = 1;
        public static int Stay_UP = 4;
        public static int Stay_DOWN = 5;
        public static int Stay_LEFT = 7;
        public static int Stay_RIGHT = 6;


        // The image representing the collection of images used for animation

        Texture2D spriteStrip;

        // The scale used to display the sprite strip

        float scale;

        // The time since we last updated the frame

        int elapsedTime;

        // The time we display a frame until the next one

        int frameTime;

        // The number of frames that the animation contains

        int frameCount;

        // The index of the current frame we are displaying

        int currentFrame;

        // The color of the frame we will be displaying

        Color color;

        // The area of the image strip we want to display

        Rectangle sourceRect = new Rectangle();

        // The area where we want to display the image strip in the game

        Rectangle destinationRect = new Rectangle();

        // Width of a given frame

        public int FrameWidth;

        // Height of a given frame

        public int FrameHeight;

        // The state of the Animation

        public bool Active;
        public bool Expire;
        // Determines if the animation will keep playing or deactivate after one run

        public bool Looping;
        private bool init;

        // Width of a given frame

        public Vector2 Position;

        private int direction;

        public int CurrentFrame { get => currentFrame; set => currentFrame = value; }
        public int FrameCount { get => frameCount; set => frameCount = value; }

        public void Initialize(Texture2D texture, Vector2 position, int frameWidth, int frameHeight, int frameCount, int frametime, Color color, float scale, bool looping)

        {

            // Keep a local copy of the values passed in

            this.color = color;
            this.FrameWidth = frameWidth;
            this.FrameHeight = frameHeight;
            this.FrameCount = frameCount;
            this.frameTime = frametime;
            this.scale = scale;



            Looping = looping;
            Position = position;
            spriteStrip = texture;



            // Set the time to zero

            elapsedTime = 0;
            CurrentFrame = 0;



            // Set the Animation to active by default

            Active = true;
            Expire = false;
            init = true;

        }

        public void Update(GameTime gameTime)

        {

            // Do not update the game if we are not active
            if (Expire)
            {
                return;
            }

            if(Active == false && Looping == false)
            {
                return;
            }

            if (Active == false) 
            {

                switch (direction)
                {
                    case 0: direction = Stay_DOWN;
                        break;
                    case 1:
                        direction = Stay_RIGHT;
                        break;
                    case 2:
                        direction = Stay_UP;
                        break;
                    case 3:
                        direction = Stay_LEFT;
                        break;
                }
            }

           

            // Update the elapsed time

            elapsedTime += (int)gameTime.ElapsedGameTime.TotalMilliseconds;
     
            // If the elapsed time is larger than the frame time
            // we need to switch frames

            if (elapsedTime > frameTime)

            {

                // Move to the next frame

                CurrentFrame++;



                // If the currentFrame is equal to frameCount reset currentFrame to zero

                if (CurrentFrame == FrameCount)

                {

                   

                    // If we are not looping deactivate the animation

                    if (Looping == false)
                    {
                        CurrentFrame -= 1;
                        Active = false;
                        Debug.WriteLine("Player animation - finished RIP on "+ Position);
                    }
                    else
                    {
                        CurrentFrame = 0;
                    }
                        

                }



                // Reset the elapsed time to zero

                elapsedTime = 0;

            }

            // Grab the correct frame in the image strip by multiplying the currentFrame index by the Frame width

            sourceRect = new Rectangle(CurrentFrame * FrameWidth, FrameHeight*direction, FrameWidth, FrameHeight);

            // Grab the correct frame in the image strip by multiplying the currentFrame index by the frame width

            destinationRect = new Rectangle((int)Position.X - (int)(FrameWidth * scale) / 2,
            (int)Position.Y - (int)(FrameHeight * scale) / 2,
            (int)(FrameWidth * scale),
            (int)(FrameHeight * scale));

        }

        // Draw the Animation Strip

        public void Draw(SpriteBatch spriteBatch)

        {
            // Only draw the animation when we are active
            if (Expire)
            {
                return;
            }
            spriteBatch.Draw(spriteStrip, destinationRect, sourceRect, color);

           
        }

        public void setDirection(int dir)
        {
            this.direction = dir;
        }
    }
}
