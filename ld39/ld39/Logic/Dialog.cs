﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueSharp;

namespace ld39.Logic
{
    class Dialog
    {
        public Vector2 Position;
        private List<Message> dialogs;
        private Message currentMessage;
        private bool Active;
        private int elapseTime;
        private SpriteFont font;
        private static Dialog DialogInstance = null;

        private Dialog(SpriteFont font)
        {
            dialogs = new List<Message>();
            Active = false;
            elapseTime = 0;
            this.font = font;
        }

        public static Dialog getInstance(SpriteFont font)
        {
            if (DialogInstance == null)
            {
                DialogInstance = new Dialog(font);
            }

            return DialogInstance;
        }

        public static Dialog getInstance()
        {
            if (DialogInstance == null)
            {
                return null;
            }

            return DialogInstance;
        }

        public void addDialog(string dialog, Color color)
        {
            dialogs.Add(new Message(dialog, color));
        }

        public void Update(GameTime gameTime, Vector2 playerPosition)
        {
            Position = playerPosition;
            Position.Y -= (Global.SpriteHeight * 1.5f);
            Position.X -= 45;
            
            if (dialogs.Count == 0)
            {
                return;
            }

            if (currentMessage == null)
            {
                currentMessage = dialogs[0];
                Active = true;
            }

            if (Active)
            {
                elapseTime += (int) gameTime.ElapsedGameTime.TotalMilliseconds;

                if (elapseTime > Global.dialogDuration)
                {
                    elapseTime = 0;
                    Active = false;
                    dialogs.RemoveAt(0);
                    currentMessage = null;
                }
            }
        }


        public void Draw(SpriteBatch sprite)
        {
            if (Active)
            {
                foreach (var dialog in dialogs)
                {
                    sprite.DrawString(font, dialog.MessageText,Position - dialog.RandomPosition, dialog.Color, 0.0f, Vector2.Zero, dialog.Scale, SpriteEffects.None, 1.0f);
                }
            }   
        }
    }
}
