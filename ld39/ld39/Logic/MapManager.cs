﻿using System;
using System.Diagnostics;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueSharp;
using RogueSharp.Random;
using TiledSharp;
using Rectangle = Microsoft.Xna.Framework.Rectangle;

namespace ld39
{
    class MapManager
    {
        private TmxMap map;
        private Texture2D tileset;
        private Map cellmap;

        int tileWidth;
        int tileHeight;
        int tilesetTilesWide;
        int tilesetTilesHigh;

        public MapManager(TmxMap map, Texture2D tileset)
        {
            this.map = map;
            this.tileset = tileset;

            tileWidth = map.Tilesets[0].TileWidth;
            tileHeight = map.Tilesets[0].TileHeight;

            tilesetTilesWide = tileset.Width / tileWidth;
            tilesetTilesHigh = tileset.Height / tileHeight;

            cellmap = new Map(map.Width, map.Height);
            initCellSet();
        }


        public void Update(Cell position)
        {
            UpdatePlayerFieldOfView(position);
        }

        public void Draw(SpriteBatch batch, Cell playerpos)
        {
            var radiusMap = cellmap.GetCellsInCircle(playerpos.X, playerpos.Y, Global.fovRadius);
            int i = 0;
            foreach (Cell cell in cellmap.GetAllCells())
            {
                Color tint = Color.White * 1f;
                if (!cell.IsExplored)
                {
                    
                    i++;
                    continue;
                }
                if (!cell.IsInFov)
                {
                    tint = Color.Gray * 0.45f;
                }

                for (int j = 0; j < map.Layers.Count; j++)
                {
                    int gid = map.Layers[j].Tiles[i].Gid;

                    // Empty tile, do nothing
                    if (gid == 0)
                    {

                    }
                    else
                    {
                        int tileFrame = gid - 1;
                        //Suche das tile aus dem tileset
                        Vector2 posTile = GetTilePosition(tileFrame);


                        float x = cell.X * tileWidth;
                        float y = cell.Y * tileHeight;

                        Rectangle tilesetRec = new Rectangle((int)(tileWidth * posTile.X), (int)(tileHeight * posTile.Y), tileWidth, tileHeight);

                        batch.Draw(tileset, new Rectangle((int)x, (int)y, tileWidth, tileHeight), tilesetRec, tint);
                    }
                }
               

                i++;
            }
        }

        public void initCellSet()
        {
            int counter = 0;
            Debug.WriteLine("0 Set Transparent Objects (" + map.ObjectGroups[0].Name + ")");
            Debug.WriteLine("1 Set Collider Objects (" + map.ObjectGroups[1].Name + ")");
            foreach (Cell cell in cellmap.GetAllCells())
            {
                //Debug.WriteLine("Betrachte Cell: ("+cell.X+","+cell.Y+")");
                int innerc = 0;
                foreach (var objects in map.ObjectGroups[1].Objects)
                {
                    Rectangle rectO = new Rectangle((int)objects.X, (int)objects.Y, (int)objects.Width, (int)objects.Height);
                    Rectangle rectC = new Rectangle((int)(cell.X * tileWidth), (int)(cell.Y*tileHeight), tileWidth, tileHeight);

                    

                    //Debug.WriteLine(rectC.ToString() + "/" + rectO.ToString());

                    //Prüfe ob das betrachtete Feld im Collider Layer liegt
                    if (rectO.Intersects(rectC))
                    {
                        cellmap.SetCellProperties(cell.X, cell.Y, false, false, false);
                        //Debug.WriteLine("Betrachte Object: (" + objects.X + "," + objects.Y + ")");
                        //Ist nicht begehbar
                        //Debug.Write("#");
                        break;
                    }
                    if (innerc == map.ObjectGroups[1].Objects.Count-1)
                    {
                        cellmap.SetCellProperties(cell.X, cell.Y, true, true, false);
                        //Ist begehbar
                        //Debug.WriteLine("Betrachte Object: (" + objects.X + "," + objects.Y + ")");
                        //Debug.Write("-");
                        break;
                    }
                    innerc++;

                }

                //Trannsparente Sicht

                foreach (var objectsTransparent in map.ObjectGroups[0].Objects)
                {
                    Rectangle rectO = new Rectangle((int)objectsTransparent.X, (int)objectsTransparent.Y, (int)objectsTransparent.Width, (int)objectsTransparent.Height);
                    Rectangle rectC = new Rectangle((int)(cell.X * tileWidth), (int)(cell.Y * tileHeight), tileWidth, tileHeight);


                    
                    //Debug.WriteLine(rectC.ToString() + "/" + rectO.ToString());

                    //Prüfe ob das betrachtete Feld im Collider Layer liegt
                    if (rectO.Intersects(rectC))
                    {
                        cellmap.SetCellProperties(cell.X, cell.Y, true, cell.IsWalkable, cell.IsExplored);
                        break;
                    }
                   
                   

                }

                counter++;

                if (counter > map.Width-1)
                {
                    counter = 0;
                   // Debug.Write("\n");
                }
            }

        }

        public Map getCellMap()
        {
            return cellmap;
        }


        private void UpdatePlayerFieldOfView(Cell position)
        {
            //Debug.WriteLine("Player {" + position.X + "," + position.Y + "}");
            //Zeichne Transparente felder um player in console (T = Transparent, S = Solid)
            /*   for(int i = 0; i < 9; i++)
            {
                Debug.WriteLine("---------------------------");
                if (cellmap.GetCell(position.X - 1, position.Y - 1).IsTransparent)
                {
                    Debug.Write("-T-");
                }
                else
                {
                    Debug.Write("-S-");
                }

                if (cellmap.GetCell(position.X, position.Y - 1).IsTransparent)
                {
                    Debug.Write("-T-");
                }
                else
                {
                    Debug.Write("-S-");
                }

                if (cellmap.GetCell(position.X + 1, position.Y - 1).IsTransparent)
                {
                    Debug.Write("-T-");
                }
                else
                {
                    Debug.Write("-S-");
                }
                Debug.Write("\n");

                if (cellmap.GetCell(position.X - 1, position.Y ).IsTransparent)
                {
                    Debug.Write("-T-");
                }
                else
                {
                    Debug.Write("-S-");
                }

                if (cellmap.GetCell(position.X, position.Y ).IsTransparent)
                {
                    Debug.Write("-T-");
                }
                else
                {
                    Debug.Write("-S-");
                }

                if (cellmap.GetCell(position.X + 1, position.Y ).IsTransparent)
                {
                    Debug.Write("-T-");
                }
                else
                {
                    Debug.Write("-S-");
                }
                Debug.Write("\n");

                if (cellmap.GetCell(position.X - 1, position.Y + 1).IsTransparent)
                {
                    Debug.Write("-T-");
                }
                else
                {
                    Debug.Write("-S-");
                }

                if (cellmap.GetCell(position.X, position.Y + 1).IsTransparent)
                {
                    Debug.Write("-T-");
                }
                else
                {
                    Debug.Write("-S-");
                }

                if (cellmap.GetCell(position.X + 1, position.Y + 1).IsTransparent)
                {
                    Debug.Write("-T-");
                }
                else
                {
                    Debug.Write("-S-");
                }
                Debug.Write("\n");
                Debug.WriteLine("---------------------------");

            }*/

            cellmap.ComputeFov((int)position.X, (int)position.Y, Global.fovRadius, true);
            foreach (Cell cell in cellmap.GetAllCells())
            {
                if (cellmap.IsInFov(cell.X, cell.Y))
                {
                    cellmap.SetCellProperties(cell.X, cell.Y, cell.IsTransparent, cell.IsWalkable, true);
                }
            }
        }


        public Cell GetRandomEmptyCell()
        {
            IRandom random = Global.Random;

            while (true)
            {
                int x = random.Next(map.Width-1);
                int y = random.Next(map.Height-1);
                if (cellmap.IsWalkable(x, y))
                {
                    return (Cell)cellmap.GetCell(x, y);
                }
            }
        }

        public Vector2 GetTilePosition(int tileframe)
        {
            //Funktionier nur wenn Player Tile Breite gleich Tileset Breite und Tileset Quadratisch
            Vector2 cell = new Vector2(0,0);
      
            if (tileframe == 0)
            {
                return cell;
            }

            while (tileframe > 0)
            {
                tileframe -= tilesetTilesWide;
                cell.Y++;
            }

            if (tileframe < 0)
            {
                cell.X = tileframe + tilesetTilesWide;
            }
            if (tileframe != 0)
            {
                cell.Y -= 1;
            }
          
            

            return cell;
        }


        public bool isOnChestCell(Cell position)
        {
            foreach (var chests in map.ObjectGroups[2].Objects)
            {
                Rectangle rectO = new Rectangle((int)chests.X, (int)chests.Y, (int)chests.Width, (int)chests.Height);
                Rectangle rectC = new Rectangle((int)(position.X * tileWidth), (int)(position.Y * tileHeight), tileWidth, tileHeight);



                //Debug.WriteLine(rectC.ToString() + "/" + rectO.ToString());

                //Prüfe ob das betrachtete Feld im Collider Layer liegt
                if (rectO.Intersects(rectC))
                {
                    map.ObjectGroups[2].Objects.Remove(chests);
                    return true;
                }



            }

            return false;
        }

        public bool isOnEndCell(Cell position)
        {
            foreach (var endtile in map.ObjectGroups[3].Objects)
            {
                Rectangle rectO = new Rectangle((int)endtile.X, (int)endtile.Y, (int)endtile.Width, (int)endtile.Height);
                Rectangle rectC = new Rectangle((int)(position.X * tileWidth), (int)(position.Y * tileHeight), tileWidth, tileHeight);



                //Debug.WriteLine(rectC.ToString() + "/" + rectO.ToString());

                //Prüfe ob das betrachtete Feld im Collider Layer liegt
                if (rectO.Intersects(rectC))
                {
                    map.ObjectGroups[2].Objects.Remove(endtile);
                    return true;
                }



            }

            return false;
        }




    }
}
