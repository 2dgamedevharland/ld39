﻿using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using RogueSharp;
using TiledSharp;
using System;
using System.Collections.Generic;
using ld39.Logic;
using ld39.Screens;
using Microsoft.Xna.Framework.Audio;
using RogueSharp.DiceNotation;

namespace ld39
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Level1 : GameScreen
    {
        GraphicsDeviceManager graphics;
        private MapManager mapManager;
        private PathToPlayer pathToPlayer;
        private Cell walkTo;
        private InputState _inputState;
        private ContentManager content;
        private List<AggressiveEnemy> _aggressiveEnemies = new List<AggressiveEnemy>();
        private Dialog dialog;
        private SoundEffect screenmusic;
        private SoundEffectInstance soundEffectInstance;


        // Represents the player

        Player player;
        HealthBarAnimation playerHealthBarAnimation;

        // Keyboard states used to determine key presses

        KeyboardState currentKeyboardState;
        KeyboardState previousKeyboardState;



        // Gamepad states used to determine button presses

        GamePadState currentGamePadState;
        GamePadState previousGamePadState;



        //Mouse states used to track Mouse button press

        MouseState currentMouseState;
        MouseState previousMouseState;

        // A movement speed for the player



        public Level1()
        {
            TransitionOnTime = TimeSpan.FromSeconds(0.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
        }

        public void ChangeScreen()
        {
            soundEffectInstance.Stop(true);
            ScreenManager.RemoveScreen(this);
            ScreenManager.AddScreen(new DeathScreen(), null);

        }
      

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        public override void LoadContent()
        {
            
            if (content == null)
            {
                content = new ContentManager(ScreenManager.Game.Services, "Content");
            }
            // Create a new SpriteBatch, which can be used to draw textures.
            TmxMap map = new TmxMap("Content/tiledmap.tmx");
            Texture2D mapsprite = content.Load<Texture2D>("map/dungeon");
            Texture2D white = content.Load<Texture2D>("map/white");
            Texture2D mouseCourser = content.Load<Texture2D>("map/Courser");

            mapManager = new MapManager(map, mapsprite);

            //TODO nehme ScreenManager Input State
            _inputState = ScreenManager.Input;
            // TODO: use this.Content to load your game content here

            // Load the player resources

            SpriteFont font = content.Load<SpriteFont>("font\\textfont");


            playerHealthBarAnimation = new HealthBarAnimation();
            Texture2D playerHealthBarTexture = content.Load<Texture2D>("player\\Leben");
            playerHealthBarAnimation.Initialize(playerHealthBarTexture, 200, 24, 4, 135, Color.White, 1f, true);

            Animation playerAnimation = new Animation();
            Texture2D playerTexture = content.Load<Texture2D>("player\\Sturmi_komplettNEU");
            playerAnimation.Initialize(playerTexture, Vector2.Zero, 64, 64, 3, 90, Color.White, 1f, true);

            Animation playerRIP = new Animation();
            Texture2D playerRIPTexture = content.Load<Texture2D>("player\\RIP");
            playerRIP.Initialize(playerRIPTexture, Vector2.Zero, 64, 64, 7, 180, Color.White, 1f, false);

            Animation Die = new Animation();
            Texture2D playerDieTexture = content.Load<Texture2D>("map\\DeathBackground");
            Die.Initialize(playerDieTexture, Global.Camera.ViewportCenter, 800, 600, 14, 250, Color.White, 1f, true);

            Cell startPosition = new Cell(4,5,true,true,true);
            player = new Player(startPosition, font)
            {
                // With a 15 armor class if the enemy has no attack bonus
                // the player will be hit 25% of the time
                ArmorClass = 5,
                AttackBonus = 1,
                // The player will roll 2D4 for damage or 2 x 4 sided Die
                // We can use the Dice class in RogueSharp for this
                Damage = 10,
                // The player can take 50 points of damage before dying
                Health = 100,
                Name = "Jack Daniels"
            };


            pathToPlayer = new PathToPlayer(player, mapManager.getCellMap(), white);
            Vector2 pos = new Vector2((int) startPosition.X * playerAnimation.FrameWidth, (int) startPosition.Y * playerAnimation.FrameHeight);

            Debug.WriteLine("Startpos.: "+ startPosition.X+","+startPosition.Y+ " --- "+ pos);
            player.Initialize(playerAnimation, pos, playerRIP, Die);
            mapManager.Update(startPosition);
            Global.Camera.CenterOnPosition(player.Position);

            AddAggressiveEnemies(60);

            Global.Camera.ViewportWidth = ScreenManager.GraphicsDevice.Viewport.Width;
            Global.Camera.ViewportHeight = ScreenManager.GraphicsDevice.Viewport.Height;
            Global.CombatManager = new CombatManager(player, _aggressiveEnemies,font);

            dialog = Dialog.getInstance(font);


            screenmusic = content.Load<SoundEffect>("sound\\IngameMusic");
            soundEffectInstance = screenmusic.CreateInstance();

            soundEffectInstance.IsLooped = true;
            soundEffectInstance.Play();

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        public override void UnloadContent()
        {
       
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
       
            _inputState = ScreenManager.Input;
            
            Global.Camera.CenterOnPosition(player.Position);

            if (_inputState.IsSpace(PlayerIndex.One))
            {
                Global.Camera.ScreenToWorld(new Vector2(player.X_Map,player.Y_Map));
            }

            if (_inputState.IsExitGame(PlayerIndex.One))
            {
                ScreenManager.Game.Exit();
            }
            if (walkTo == null)
            {
                walkTo = player.PlayerPosition;
            }
            
            //Update the Map FOV 
            mapManager.Update(player.PlayerPosition);


           

           
            //Calculate path to the last mouse click
            if (_inputState.IsNewLeftMouseClick(out currentMouseState))
            {
                walkTo = _inputState.GetMousePositionCell(new Vector2(currentMouseState.X, currentMouseState.Y),mapManager.getCellMap(),player.Width);
            }

            pathToPlayer.CreateFrom(walkTo.X, walkTo.Y);
           
            foreach (var enemy in _aggressiveEnemies)
            {
                enemy.Update(gameTime);
            }

            //Update the player
            player.UpdatePlayer(gameTime, _inputState, mapManager.getCellMap());

            if (mapManager.isOnChestCell(player.PlayerPosition))
            {
                string gift = player.earnRandomGift();
                Debug.WriteLine("Got Gift: "+gift);
                dialog.addDialog(gift,Color.DarkBlue);
            }

            if (player.Dead)
            {
                ChangeScreen();
            }
           
            if (mapManager.isOnEndCell(player.PlayerPosition))
            {
                gameWonScreen();
            }
            Vector2 cameraLeftUpperCorner = new Vector2(player.Position.X - (Global.Camera.ViewportWidth / 2) + (Global.SpriteWidth * 2f), player.Position.Y - (Global.Camera.ViewportHeight / 2) + Global.SpriteHeight);
            //playerHealthBarAnimation.Position = cameraLeftUpperCorner;
            playerHealthBarAnimation.Update(gameTime, player.Health);


            dialog.Update(gameTime,player.Position);

           
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
        }

    

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {
      
            var spriteBatch = ScreenManager.SpriteBatch;

           
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend,
                null, null, null, null, Global.Camera.TranslationMatrix);


            // TODO: Add your drawing code here
           
            mapManager.Draw(spriteBatch, player.PlayerPosition);
            //pathToPlayer.Draw(spriteBatch);
            foreach (var enemy in _aggressiveEnemies)
            {
                if (mapManager.getCellMap().IsInFov(enemy.X, enemy.Y))
                {
                    enemy.Draw(spriteBatch);
                }
            }
            player.Draw(spriteBatch,gameTime);
            dialog.Draw(spriteBatch);
           
            _inputState.DrawMouseCourser(spriteBatch);
            
            spriteBatch.End();


            //Male HealthBar unabhängig von Cameraposition
            spriteBatch.Begin();
            playerHealthBarAnimation.Draw(spriteBatch);
            spriteBatch.End();


            base.Draw(gameTime);
        }

        // Create a new method that will add a bunch of enemies to the game
        private void AddAggressiveEnemies(int numberOfEnemies)
        {
            for (int i = 0; i < numberOfEnemies; i++)
            {
                // Find a new empty cell for each enemy
                Cell enemyCell = mapManager.GetRandomEmptyCell();
                Vector2 pos = new Vector2((int)enemyCell.X * Global.SpriteWidth, (int)enemyCell.Y * Global.SpriteHeight);
                var pathFromAggressiveEnemy =
                    new PathToPlayer(player, mapManager.getCellMap(), content.Load<Texture2D>("map\\white"));
                pathFromAggressiveEnemy.CreateFrom(enemyCell.X, enemyCell.Y);
                Texture2D enemyTexture2D = content.Load<Texture2D>("player\\Snake");
                Animation enemyAnimation = new Animation();
                enemyAnimation.Initialize(enemyTexture2D, pos, 45, 80, 32, 90, Color.White, 1.0f, true);

                var enemy = new AggressiveEnemy(mapManager.getCellMap(), pathFromAggressiveEnemy, enemyAnimation)
                {
                    X = enemyCell.X,
                    Y = enemyCell.Y,
                    // Hounds will get hit 50% of the time with no attack bonus
                    ArmorClass = 10,
                    AttackBonus = 0,
                    // Hounds roll one 3 sided Die for damage
                    Damage = 10,
                    Health = 20,
                    Name = "Snake"
                };
                // Add each enemy to list of enemies
                _aggressiveEnemies.Add(enemy);
            }
        }

        private void gameWonScreen()
        {
            soundEffectInstance.Stop(true);
            ScreenManager.RemoveScreen(this);
            ScreenManager.AddScreen(new EndScreen(), null);
        }
    }


   
}
