﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;
using RogueSharp;

namespace ld39
{
    class StartScreen : GameScreen
    {
        public Texture2D Start;
        private ContentManager content;
        private InputState inputState;
        private SoundEffect  screenmusic;
        private SoundEffectInstance soundEffectInstance;

        public Vector2 StartPosition;

        public StartScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(0.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
        }

        public override void LoadContent()
        {
            if (content == null)
            {
                content = new ContentManager(ScreenManager.Game.Services, "Content");
            }

            screenmusic = content.Load<SoundEffect>("sound\\TitleMusic");
            soundEffectInstance = screenmusic.CreateInstance();

            soundEffectInstance.IsLooped = true;
            soundEffectInstance.Play();
            



            Start = content.Load<Texture2D>("map\\background");
            inputState = ScreenManager.Input;
            base.LoadContent();
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            inputState = ScreenManager.Input;

            if (inputState.IsExitGame(PlayerIndex.One))
            {
               ScreenManager.Game.Exit();
            }
            //Wenn Spiel starten soll
            if (inputState.IsSpace(PlayerIndex.One))
            {
                soundEffectInstance.Stop(true);
                ScreenManager.RemoveScreen(this);
                ScreenManager.AddScreen(new Level1(), null);
            }

            if (inputState.IsUp(PlayerIndex.One))
            {
                Game.toggleFullScreen();
            }


            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
        }

        

        public override void Draw(GameTime gameTime)

        {
            var spriteBatch = ScreenManager.SpriteBatch;
            spriteBatch.Begin();


            spriteBatch.Draw(Start, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);

            spriteBatch.End();


             base.Draw(gameTime);
        }
    }
}
