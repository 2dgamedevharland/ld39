﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using RogueSharp;

namespace ld39
{
    class DeathScreen : GameScreen
    {
        public Texture2D Death;
        private ContentManager content;
        private InputState inputState;
        private Cell playerposition;
        private Animation Die;
        private SoundEffect screenmusic;
        private SoundEffectInstance soundEffectInstance;


        private Vector2 DeathPosition;

        public DeathScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(0.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
        }
        
        public override void LoadContent()
        {
            if (content == null)
            {
                content = new ContentManager(ScreenManager.Game.Services, "Content");
            }

            Die = new Animation();
            Texture2D playerDieTexture = content.Load<Texture2D>("map\\DeathBackground");
            Die.Initialize(playerDieTexture, Global.Camera.ViewportCenter, 800, 600, 14, 250, Color.White, 1f, false);

            Death = content.Load<Texture2D>("map\\hound");
            inputState = ScreenManager.Input;


            screenmusic = content.Load<SoundEffect>("sound\\DeathMusic");
            soundEffectInstance = screenmusic.CreateInstance();

            soundEffectInstance.IsLooped = true;
            soundEffectInstance.Play();


            base.LoadContent();
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            inputState = ScreenManager.Input;
            

            //Wenn Spiel starten soll
            if (inputState.IsR(PlayerIndex.One))
            {
                soundEffectInstance.Stop(true);
                ScreenManager.RemoveScreen(this);
                ScreenManager.AddScreen(new Level1(), null);
            }
            else if (inputState.IsExitGame(PlayerIndex.One))
            {
                soundEffectInstance.Stop(true);
                ScreenManager.RemoveScreen(this);
                ScreenManager.AddScreen(new StartScreen(), null);

            }

            Die.Update(gameTime);
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
        }



        public override void Draw(GameTime gameTime)

        {
            var spriteBatch = ScreenManager.SpriteBatch;
            spriteBatch.Begin();
            spriteBatch.Draw(Death, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            Die.Draw(spriteBatch);
           

            spriteBatch.End();


            base.Draw(gameTime);
        }
    }
}
