﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace ld39.Screens
{
    class EndScreen :GameScreen
    {
        private ContentManager content;
        private Animation endanimation;
        private SoundEffect screenmusic;
        private SoundEffectInstance soundEffectInstance;

        public EndScreen()
        {
            
        }

        public override void LoadContent()
        {
            if (content == null)
            {
                content = new ContentManager(ScreenManager.Game.Services, "Content");
            }

            Texture2D end = content.Load<Texture2D>("map\\endsprite");
            endanimation = new Animation();
            endanimation.Initialize(end,new Vector2(400,300),800,600,10,90,Color.White,1.0f,true );


            screenmusic = content.Load<SoundEffect>("sound\\EndMusic");
            soundEffectInstance = screenmusic.CreateInstance();

            soundEffectInstance.IsLooped = true;
            soundEffectInstance.Play();


            base.LoadContent();
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            if (ScreenManager.Input.IsExitGame(PlayerIndex.One))
            {
                ScreenManager.Game.Exit();
            }

            endanimation.Update(gameTime);
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
        }

        public override void Draw(GameTime gameTime)
        {
            var spriteBatch = ScreenManager.SpriteBatch;

            spriteBatch.Begin();
            endanimation.Draw(spriteBatch);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
