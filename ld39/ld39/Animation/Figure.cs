﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using RogueSharp.DiceNotation;

namespace ld39
{
    // The new figure class has common properties shared
    // by both enemy and player classes
    public class Figure
    {
        public int X_Cell { get; set; }
        public int Y_Cell { get; set; }
        public int X_Map { get; set; }
        public int Y_Map { get; set; }
        public Animation Sprite { get; set; }

        // Roll a 20-sided die and add this value when making an attack
        public int AttackBonus { get; set; }
        // An attack must meet or exceed this value to hit
        public int ArmorClass { get; set; }
        // Roll these dice to determine how much damage was dealt after a hit
        public int Damage { get; set; }
        // How many points of damage the figure can withstand before dieing
        public int Health { get; set; }
        // The name of the figure, used for attack messages
        public string Name { get; set; }
    }
}
