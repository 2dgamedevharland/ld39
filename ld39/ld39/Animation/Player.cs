﻿using System.Diagnostics;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using RogueSharp;


namespace ld39
{
    public class Player : Figure
    {
        //Cell to move to
        private Cell playerPosition;

        // A movement speed for the player

        float playerMoveSpeed;


        // Animation representing the player

        public Animation PlayerAnimation;
        public Animation PlayerRIP;
        public Animation Die;


        // Position of the Player relative to the upper left side of the screen

        public Vector2 Position;


        // State of the player

        public bool Active;
        private SpriteFont font;


        private int elapsedTime;
        private int elapsedTimeAttack;

        public int Out
        {
            get { return 0; }
        }

        // Get the width of the player ship

        public int Width

        {
            get { return PlayerAnimation.FrameWidth; }
        }


        // Get the height of the player ship

        public int Height

        {
            get { return PlayerAnimation.FrameHeight; }
        }

        public Cell PlayerPosition
        {
            get => playerPosition;
            set => playerPosition = value;
        }
        public bool Dead { get; internal set; }

        public Player(Cell playerPosition, SpriteFont font)
        {
            playerMoveSpeed = Global.PlayerMovementSpeed;
            this.font = font;
            PlayerPosition = playerPosition;
            Dead = false;
        }


        public void getDMG(int i)
        {
            Health -= i;
        }

        public void heal(int i)
        {
            Health += i;
        }

        public void RIP(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (Health <= 0)
            {   
                
                PlayerAnimation.Expire = true;
                PlayerRIP.Position = new Vector2(X_Map,Y_Map);
                PlayerRIP.Draw(spriteBatch);

                elapsedTime += (int)gameTime.ElapsedGameTime.TotalMilliseconds;
                Die.Position = Global.Camera.ViewportCenter;
                //Die.Draw(spriteBatch);

                if (elapsedTime > 1500)
                {
                    Dead = true;
                    elapsedTime = 0;
                }

            }
        }

        public void Initialize(Animation animation, Vector2 position, Animation Animation, Animation dieanimation)

        {
            PlayerAnimation = animation;
            PlayerRIP = Animation;
            Die = dieanimation;
            // Set the starting position of the player around the middle of the screen and to the back

            Position = position;

            // Set the player to be active

            Active = true;

            // Set the player health

          
        }

        // Update the player animation


        private void Update(GameTime gameTime)
        {
            PlayerAnimation.Position = Position;
            PlayerAnimation.Update(gameTime);
            if (Health <= 0)
            {
                PlayerRIP.Update(gameTime);
                Die.Update(gameTime);

            }
           
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)

        {
            RIP(spriteBatch, gameTime);
            PlayerAnimation.Draw(spriteBatch);

            Vector2 namePos = Position;
            namePos.Y -= Global.SpriteHeight - 20;
            namePos.X -= (Global.SpriteWidth / 2);
            spriteBatch.DrawString(font,Name, namePos, Color.Gray,0.0f,Vector2.Zero,0.5f,SpriteEffects.None,1.0f);
        }

        public void UpdatePlayer(GameTime gameTime, InputState inputState, Map cellMap)
        {
            PlayerAnimation.Active = false;
            Vector2 previousPosition = Position;
            KeyboardState keyboardState = inputState.CurrentKeyboardStates[0];
            playerPosition = CurrentPlayerCell(cellMap);
            X_Cell = playerPosition.X;
            Y_Cell = playerPosition.Y;
            X_Map = (int) Position.X;
            Y_Map = (int) Position.Y;

            if (Health > 100)
            {
                Health = 100;
            }

            if (Health < 25)
            {
                playerMoveSpeed = 2.0f;
            }
            else
            {
                playerMoveSpeed = Global.PlayerMovementSpeed;
            }

            if (Health < 10)
            {
                playerMoveSpeed = 0.75f;
            }

            //Wenn tot, bleib da
            if (Health <= 0)
            {
                Update(gameTime);
                return;
            }
            // Update the elapsed time

            elapsedTimeAttack += (int) gameTime.ElapsedGameTime.TotalMilliseconds;

            if (elapsedTimeAttack > Global.PlayerAttackTime)
            {
                Health -= Global.Poison;
                
                elapsedTimeAttack = 0;
                var enemy = Global.CombatManager.EnemyAt(X_Cell, Y_Cell);
                if (enemy == null)
                {
                }
                else
                {
                    // When there is an enemy in the cell, make an
                    // attack against them by using the CombatManager
                    Global.CombatManager.Attack(this, enemy);
                }
            }
            // Use the Keyboard 

            if (keyboardState.IsKeyDown(Keys.Left))
            {
                PlayerAnimation.Active = true;
                PlayerAnimation.setDirection(Animation.MOVE_LEFT);
                Position.X -= playerMoveSpeed;
            }


            if (keyboardState.IsKeyDown(Keys.Right))

            {
                PlayerAnimation.Active = true;
                PlayerAnimation.setDirection(Animation.MOVE_RIGHT);
                Position.X += playerMoveSpeed;
            }


            if (keyboardState.IsKeyDown(Keys.Up))

            {
                PlayerAnimation.Active = true;
                PlayerAnimation.setDirection(Animation.MOVE_UP);
                Position.Y -= playerMoveSpeed;
            }


            if (keyboardState.IsKeyDown(Keys.Down))

            {
                PlayerAnimation.Active = true;
                PlayerAnimation.setDirection(Animation.MOVE_DOWN);
                Position.Y += playerMoveSpeed;
            }


            if (!CurrentPlayerCell(cellMap).IsWalkable)
            {
                Position = previousPosition;
            }

            Update(gameTime);
        }


        public Cell CurrentPlayerCell(Map cellMap)
        {
            //Funktionier nur wenn Player Tile Breite gleich Tileset Breite und Tileset Quadratisch
            Cell playerCell;

            int x = (int) Position.X;
            int colum = 0;
            if (x < 0)
            {
                colum = 0;
            }
            while (x > 0)
            {
                if (x >= 0)
                {
                    colum++;
                }

                x -= Global.SpriteWidth;
            }
            colum -= 1;

            int y = (int) Position.Y;
            int row = 0;
            if (y < 0)
            {
                row = 0;
            }
            while (y > 0)
            {
                if (y >= 0)
                {
                    row++;
                }

                y -= Global.SpriteWidth;
            }
            row -= 1;

            playerCell = (Cell) cellMap.GetCell(colum, row);
            // Debug.WriteLine("Player {"+playerCell.X+","+playerCell.Y+"}");
            return playerCell;
        }

        public string earnRandomGift()
        {
            int i;
            i = Global.Random.Next(7);

            switch (i)
            {
                case 0:
                    heal(40);
                    return "Health +40";
                case 1:
                    Damage += 5;
                    return "Damage +5";
                case 2:
                    heal(50);
                    return "Health +50";
                case 3:
                    ArmorClass += 4;
                    return "Amor +4";
                case 4:
                    Name = "Sir " + Name;
                    return "Got TITLE -SIR-";
                case 5:
                    AttackBonus += 4;
                    return "Attack Bonus +4";
                case 6:
                    heal(40);
                    return "Health +40";
                case 7:
                    Global.PlayerMovementSpeed += 1.5f;
                    return "Speed shoes";
                default:
                    return "Empty chest";
            }

            return "";
        }
    }
}