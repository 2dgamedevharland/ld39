﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueSharp;

namespace ld39
{
    public class AggressiveEnemy : Figure
    {
        private readonly PathToPlayer _path;
        public int X { get; set; }
        public int Y { get; set; }
        public float Scale { get; set; }
        public Animation Sprite { get; set; }
        private readonly IMap _map;
        private bool _isAwareOfPlayer;
        int elapsedTime;
        

        public AggressiveEnemy(IMap map,PathToPlayer path, Animation animation)
        {
            Sprite = animation;
            _path = path;
            _map = map;
            elapsedTime = 0;
             Sprite.CurrentFrame = Global.Random.Next(Sprite.FrameCount);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            Sprite.Draw(spriteBatch);
        }

        public void Update(GameTime gameTime)
        {
         
            elapsedTime += (int)gameTime.ElapsedGameTime.TotalMilliseconds;
            X_Cell = X;
            Y_Cell = Y;
            X_Map = (int)X * Global.SpriteWidth;
            Y_Map = (int)Y * Global.SpriteHeight;

            if (elapsedTime > Global.enemyMovingTime)
            {
                elapsedTime = 0;

                if (!_isAwareOfPlayer)
                {
                    // When the enemy is not aware of the player
                    // check the map to see if they are in field-of-view
                    
                    if (_map.IsInFov(X,Y))
                    {
                        _isAwareOfPlayer = true;
                    }
                }
                // Once the enemy is aware of the player
                // they will never lose track of the player
                // and will pursue relentlessly
                if (_isAwareOfPlayer)
                {
                    _path.CreateFrom(X, Y);
                    // Use the CombatManager to check if the player located
                    // at the cell we are moving into
                    if (Global.CombatManager.IsPlayerAt(_path.FirstCell.X, _path.FirstCell.Y))
                    {
                        // Make an attack against the player
                        Global.CombatManager.Attack(this,
                        Global.CombatManager.FigureAt(_path.FirstCell.X, _path.FirstCell.Y));
                    }
                    else
                    {
                        // Since the player wasn't in the cell, just move into as normal
                        X = _path.NextStepCell.X;
                        Y = _path.NextStepCell.Y;
                    }
                }

               

                //TODO Walk to Player
            }

            Sprite.Position.X = X * Global.SpriteWidth + (Global.SpriteWidth/2);
            Sprite.Position.Y = Y * Global.SpriteHeight;

            Sprite.Update(gameTime);
        }

       
    }


  
}

