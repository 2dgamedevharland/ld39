﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;

namespace ld39
{
    class HealthBarAnimation
    {
        // The image representing the collection of images used for animation

        Texture2D spriteStrip;

        // The scale used to display the sprite strip

        float scale;

        // The time since we last updated the frame

        int elapsedTime;

        // The time we display a frame until the next one

        int frameTime;

        // The number of frames that the animation contains

        int frameCount;

        // The index of the current frame we are displaying

        int currentFrame;

        // The color of the frame we will be displaying

        Color color;

        // The area of the image strip we want to display

        Rectangle sourceRect = new Rectangle();

        // The area where we want to display the image strip in the game

        Rectangle destinationRect = new Rectangle();

        // Width of a given frame

        public int FrameWidth;
        public int FrameWidth_HB;

        // Height of a given frame

        public int FrameHeight;

        // The state of the Animation

        public bool Active;

        // Determines if the animation will keep playing or deactivate after one run

        public bool Looping;
        private bool init;

        // Width of a given frame

        public Vector2 Position;

        private double HP;


        public void Initialize(Texture2D texture, int frameWidth, int frameHeight, int frameCount, int frametime,
            Color color, float scale, bool looping)

        {
            // Keep a local copy of the values passed in

            this.color = color;
            this.FrameWidth = frameWidth;
            this.FrameHeight = frameHeight;
            this.frameCount = frameCount;
            this.frameTime = frametime;
            this.scale = scale;
            this.FrameWidth_HB = frameWidth;


            Looping = looping;
            Position = new Vector2(120, 63);
            spriteStrip = texture;
            HP = (FrameWidth / 100.0d);


            // Set the time to zero

            elapsedTime = 0;
            currentFrame = 0;


            // Set the Animation to active by default

            Active = true;
        }

        public void Update(GameTime gameTime, int health)

        {
            // Update the elapsed time

          

            elapsedTime += (int) gameTime.ElapsedGameTime.TotalMilliseconds;


            if (elapsedTime > frameTime)

            {
                currentFrame++;


                // If the currentFrame is equal to frameCount reset currentFrame to zero

                if (currentFrame == frameCount)

                {
                    currentFrame = 0;

                    // If we are not looping deactivate the animation

                    if (Looping == false)
                        Active = false;
                }


                FrameWidth_HB = (int) (health * HP * 1.0d);

                // Reset the elapsed time to zero

                elapsedTime = 0;


                // Grab the correct frame in the image strip by multiplying the currentFrame index by the Frame width

                sourceRect = new Rectangle(currentFrame * FrameWidth, 0, FrameWidth_HB, FrameHeight);

                // Grab the correct frame in the image strip by multiplying the currentFrame index by the frame width

                destinationRect = new Rectangle((int) Position.X - (int) (FrameWidth * scale) / 2,
                    (int) Position.Y - (int) (FrameHeight * scale) / 2,
                    (int) (FrameWidth_HB * scale),
                    (int) (FrameHeight * scale));
            }
        }

        // Draw the Animation Strip

        public void Draw(SpriteBatch spriteBatch)

        {
            // Only draw the animation when we are active
            spriteBatch.Draw(spriteStrip, destinationRect, sourceRect, color);
        }
    }
}