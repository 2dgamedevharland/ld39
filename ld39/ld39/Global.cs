﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ld39.Screens;
using Microsoft.Xna.Framework.Graphics;
using RogueSharp.Random;

namespace ld39
{

    public enum GameStates
    {
        None = 0,
        PlayerTurn = 1,
        EnemyTurn = 2,
        Debugging = 3
    }
   
       
   
    class Global
    {
        public static GameStates GameState { get; set; }
        public static float PlayerMovementSpeed = 3f;


        public static int Poison = 1;

        public static readonly Camera Camera = new Camera();
        public static readonly IRandom Random = new DotNetRandom();
        // In Global.cs add the following.
        public static readonly int MapWidth = 100;
        public static readonly int MapHeight = 100;
        public static readonly int SpriteWidth = 64;
        public static readonly int SpriteHeight = 64;
        public static readonly int fovRadius = 6;
        public static readonly int enemyMovingTime = 1000;
        public static CombatManager CombatManager;
        public static readonly int PlayerAttackTime = 900;
        public static readonly int dialogDuration = 700;
    }
}
